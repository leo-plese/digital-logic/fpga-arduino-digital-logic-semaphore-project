# FPGA Arduino Digital Logic Semaphore Project

High school project in Computer Science. Making a Digital Logic (C in Arduino DEV, FPGA HDL schematic language Lattice Diamond tool) app for simulation of traffic light regulation.

Implemented in C and a HDL language.

Created: 2016